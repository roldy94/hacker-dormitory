import React, { useState, useEffect } from 'react';

// `joiningDate` && `validityDate` format "yyyy-mm-dd"

function checkValidity(joiningDate, validityDate) {
  const now = new Date();
  const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  const [year, month, day] = joiningDate.split('-');
  const [yyyy, mm, dd] = validityDate.split('-');
  const maxValid = new Date(yyyy, mm - 1, dd);
  const selected = new Date(year, month - 1, day);
  return maxValid >= selected && maxValid >= today;
}

function searchStudents(students, name) {
  let index = -1;
  let current = null;
  students.map(function (student, i, array) {
    if (student.name.toUpperCase() === name.toUpperCase()) {
      index = i;
      current = student;
    }
  });
  return { index, current };
}

function _handleOnClick(
  name,
  date,
  students,
  handleCurrent,
  handleName,
  handleDate,
  handleShow,
  handleMessage
) {
  handleShow(false);

  if (name === '' || date === '') {
    handleShow(true);
    handleMessage('Por favor complete todo los campos');
  } else {
    const { index, current } = searchStudents(students, name);
    if (index >= 0) {
      if (checkValidity(date, current.validityDate)) {
        //add ul
        handleCurrent(current);
        handleShow(false);
      } else {
        //add msg error
        handleShow(true);
        handleMessage('Sorry, ' + current.name + '\'s validity has Expired!');
      }
    } else {
      //add msg error
      handleShow(true);
      handleMessage('Sorry, '+ name +' is not a verified student!');
    }
    handleDate('');
    handleName('');
  }
}

function Search({
  students,
  name,
  date,
  handleDate,
  handleName,
  handleCurrent,
  handleShow,
  handleMessage,
}) {
  return (
    <div className='my-50 layout-row align-items-end justify-content-end'>
      <label htmlFor='studentName'>
        Student Name:
        <div>
          <input
            id='studentName'
            data-testid='studentName'
            type='text'
            className='mr-30 mt-10'
            value={name}
            onChange={(e) => handleName(e.target.value)}
          />
        </div>
      </label>
      <label htmlFor='joiningDate'>
        Joining Date:
        <div>
          <input
            id='joiningDate'
            data-testid='joiningDate'
            type='date'
            value={date}
            className='mr-30 mt-10'
            onChange={(e) => handleDate(e.target.value)}
          />
        </div>
      </label>
      <button
        type='button'
        data-testid='addBtn'
        className='small mb-0'
        onClick={() =>
          _handleOnClick(
            name,
            date,
            students,
            handleCurrent,
            handleName,
            handleDate,
            handleShow,
            handleMessage
          )
        }
      >
        Add
      </button>
    </div>
  );
}

export default Search;
