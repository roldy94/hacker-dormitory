import React, { useState, useCallback } from 'react';
import './App.css';
import ResidentsList from './Components/ResidentsList';
import Search from './Components/Search';
import Error from './Components/Error';
import 'h8k-components';
import { STUDENTS } from './studentsList.js';

const title = 'Challenge';

function App() {
  const [currentList, setCurrentList] = useState([]);
  const [name, setName] = useState('');
  const [date, setDate] = useState('');
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState('');

  const _handleName = (value) => {
    setName(value);
  };
  const _handleDate = (value) => {
    setDate(value);
  };
  const _handleCurrent = (value) => {
    setCurrentList([
      ...currentList,
      {
        name: value.name,
        validityDate: value.validityDate,
      },
    ]);
  };
  const _handleShow = (value) => {
    setShow(value);
  };
  const _handleMessage = (value) => {
    setMessage(value);
  };

  return (
    <div className='App'>
      <h8k-navbar header={title}></h8k-navbar>
      <div className='layout-column justify-content-center align-items-center w-50 mx-auto'>
        <Search
          students={STUDENTS}
          name={name}
          date={date}
          handleDate={_handleDate}
          handleName={_handleName}
          handleCurrent={_handleCurrent}
          handleShow={_handleShow}
          handleMessage={_handleMessage}
        />
        {show ? <Error message={message} /> : null}
        <ResidentsList list={currentList} />
      </div>
    </div>
  );
}

export default App;
